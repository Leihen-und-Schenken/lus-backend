import toml

from os.path import exists

from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import current_user, JWTManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from oso import Oso
from sqlalchemy_oso import register_models
from sqlalchemy_oso.flask import AuthorizedSQLAlchemy


__version__ = "0.0.1"
app = Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
if exists("/etc/leihenundschenken/backend.toml"):
    app.config.from_file("/etc/leihenundschenken/backend.toml", load=toml.load)
app.config.from_prefixed_env()

oso = Oso()
app.oso = oso
oso.load_files(["leihenundschenken/policy.polar"])

db = SQLAlchemy(app)

migrate = Migrate(app, db)
cors = CORS(app, resources={"/graphql": {"origins": "*"}})
jwt = JWTManager(app)


@jwt.user_identity_loader
def user_identity_lookup(user):
    return user.uuid


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]
    return User.query.filter_by(uuid=identity).one_or_none()

from .models import *
from .views import *

register_models(oso, db.Model)
