from datetime import datetime, timedelta
from uuid import uuid4

from sqlalchemy.dialects.postgresql import UUID
from werkzeug.security import check_password_hash

from leihenundschenken import app, db


class Item(db.Model):
    uuid = db.Column(UUID(as_uuid=True), primary_key=True, unique=True, default=uuid4)
    category = db.Column(db.String(), nullable=False)
    status = db.Column(db.String(), nullable=False)
    owner = db.Column(db.String(), nullable=False)
    holder = db.Column(db.String(), nullable=False)
    title = db.Column(db.String(), nullable=False)
    description = db.Column(db.String(), nullable=True)
    tags = db.relationship("Tag", secondary="item_tag", back_populates="items", lazy=True)


class Tag(db.Model):
    pk = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(), nullable=False)
    value = db.Column(db.String(), nullable=False)
    items = db.relationship("Item", secondary="item_tag", back_populates="tags", lazy=True)


class User(db.Model):
    uuid = db.Column(UUID(as_uuid=True), primary_key=True, unique=True, default=uuid4)
    username = db.Column(db.String(), nullable=False)
    email = db.Column(db.String(), nullable=False)
    password = db.Column(db.String(), nullable=False)
    postcode = db.Column(db.Integer, nullable=False)
    active = db.Column(db.Boolean(), nullable=False)

    def login(self, password):
        return self.active and check_password_hash(self.password, password)


class RegistrationRequest(db.Model):
    user_uuid = db.Column(UUID(as_uuid=True), db.ForeignKey("user.uuid"), primary_key=True)
    token = db.Column(db.String(), nullable=False)
    created = db.Column(db.DateTime(), nullable=False)

    def is_expired(self):
        valid = self.created + timedelta(hours=app.config["LUS_REGISTRATION_REQUEST_VALID_HOURS"])
        return datetime.now() > valid


class ItemTag(db.Model):
    __tablename__ = "item_tag"
    item_uuid = db.Column(UUID(as_uuid=True), db.ForeignKey("item.uuid"), primary_key=True)
    tag_id = db.Column(db.Integer, db.ForeignKey("tag.pk"), primary_key=True)
