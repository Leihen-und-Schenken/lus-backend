allow(user, action, _) if
    action == "read" and user.username != "anonymous";

allow(user, action, object) if
    (action == "delete" or action == "update") and object.owner == user.username;
