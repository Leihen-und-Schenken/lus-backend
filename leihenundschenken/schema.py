from datetime import datetime, timedelta
from email.message import EmailMessage
from email.utils import localtime
import secrets
from smtplib import SMTP
from uuid import UUID

import graphene
from graphene import relay
from graphene_sqlalchemy import SQLAlchemyObjectType, SQLAlchemyConnectionField
from flask_jwt_extended import create_access_token
from flask_jwt_extended import current_user
from flask_jwt_extended import jwt_required
from sqlalchemy import or_
from sqlalchemy_oso.auth import authorize_model
from werkzeug.security import generate_password_hash

from leihenundschenken import app, db, oso
from .models import Item, Tag, User, RegistrationRequest


def get_user_by_name_or_email(info, identifier):
    return UserType.get_query(info).filter(or_(User.username == identifier, User.email == identifier)).first()


class FrontendMenuItem(graphene.ObjectType):
    slug = graphene.String()


class ItemType(SQLAlchemyObjectType):
    class Meta:
        model = Item
        interfaces = (relay.Node,)


class TagType(SQLAlchemyObjectType):
    class Meta:
        model = Tag


class UserType(SQLAlchemyObjectType):
    class Meta:
        model = User


class UserPasswordAuthenticationMutation(graphene.Mutation):
    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)

    success = graphene.Field(graphene.Boolean)
    access_token = graphene.Field(graphene.String)

    @classmethod
    def mutate(
        cls,
        root,
        info,
        username,
        password
    ):
        user = User.query.filter_by(username=username).first()
        if not user or not user.login(password):
            return UserPasswordAuthenticationMutation(success=False, access_token=None)

        access_token = create_access_token(identity=user)
        return UserPasswordAuthenticationMutation(success=True, access_token=access_token)


class UserRegistrationRequestMutation(graphene.Mutation):
    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)
        postcode = graphene.Int(required=True)

    success = graphene.Field(graphene.Boolean)

    @classmethod
    def mutate(
        cls,
        root,
        info,
        username,
        password,
        email,
        postcode
    ):
        existing_user = UserType.get_query(info).filter(or_(User.username==username, User.email==email)).first()
        if existing_user:
            return UserRegistrationRequestMutation(success=False)
        user = User(
            username=username,
            password=generate_password_hash(password),
            email=email,
            postcode=postcode,
            active=False
        )
        db.session.add(user)
        db.session.commit()

        token = secrets.token_urlsafe()
        registration_request = RegistrationRequest(
            user_uuid=user.uuid,
            token=token,
            created=datetime.now()
        )

        msg = EmailMessage()
        msg.set_content(app.config["LUS_REGISTRATION_EMAIL_BODY"].format(username=username, token=token))
        msg["Date"] = localtime()
        msg["Subject"] = app.config["LUS_REGISTRATION_EMAIL_SUBJECT"]
        msg["From"] = app.config["LUS_SMTP_FROM"]
        msg["To"] = email

        smtp = SMTP(app.config["LUS_SMTP_HOST"])
        if "LUS_SMTP_STARTTLS" in app.config and app.config["LUS_SMTP_STARTTLS"]:
            smtp.starttls()
        if "LUS_SMTP_USER" in app.config:
            smtp.login(app.config["LUS_SMTP_USER"], app.config["LUS_SMTP_PASSWORD"])
        smtp.send_message(msg)

        db.session.add(registration_request)
        db.session.commit()

        return UserRegistrationRequestMutation(success=True)


class UserRegistrationMutation(graphene.Mutation):
    class Arguments:
        token = graphene.String(required=True)

    success = graphene.Field(graphene.Boolean)

    @classmethod
    def mutate(
        cls,
        root,
        info,
        token
    ):
        registration_request = RegistrationRequest.query.filter(RegistrationRequest.token==token).first()
        if not registration_request or registration_request.is_expired():
            return UserRegistrationMutation(success=False)
        user = User.query.get(registration_request.user_uuid)
        user.active = True
        db.session.add(user)
        db.session.delete(registration_request)
        db.session.commit()
        return UserRegistrationMutation(success=True)


class CreateItemMutation(graphene.Mutation):
    class Arguments:
        category = graphene.String(required=True)
        status = graphene.String(required=True)
        title = graphene.String(required=True)
        description = graphene.String()

    item = graphene.Field(ItemType)

    @classmethod
    @jwt_required()
    def mutate(
        cls,
        root,
        info,
        category,
        status,
        title,
        description=None
    ):
        if category != "" and status != "" and title != "":
            item = Item(category=category, status=status, owner=current_user.username, holder=current_user.username, title=title, description=description)
            db.session.add(item)
            db.session.commit()
            return CreateItemMutation(item=item)


class DeleteItemMutation(graphene.Mutation):
    class Arguments:
        uuid = graphene.ID(required=True)

    success = graphene.Field(graphene.Boolean)

    @classmethod
    @jwt_required()
    def mutate(
        cls,
        root,
        info,
        uuid
    ):
        uuid = UUID(uuid)
        auth_filter = authorize_model(oso, current_user, "delete", db.session, Item)
        item = ItemType.get_query(info).filter(auth_filter).filter(Item.uuid == uuid).first()
        if item:
            db.session.delete(item)
            db.session.commit()
            return DeleteItemMutation(success=True)
        return DeleteItemMutation(success=False)


class UpdateItemMutation(graphene.Mutation):
    class Arguments:
        uuid = graphene.ID(required=True)
        category = graphene.String()
        status = graphene.String()
        title = graphene.String()
        description = graphene.String()
        owner = graphene.String()
        holder = graphene.String()

    success = graphene.Field(graphene.Boolean)

    @classmethod
    @jwt_required()
    def mutate(
        cls,
        root,
        info,
        uuid,
        category=None,
        status=None,
        title=None,
        description=None,
        owner=None,
        holder=None
    ):
        uuid = UUID(uuid)
        auth_filter = authorize_model(oso, current_user, "update", db.session, Item)
        item = ItemType.get_query(info).filter(auth_filter).filter(Item.uuid == uuid).first()
        if item:
            if category:
                item.category = category
            if status:
                item.status = status
            if title:
                item.title = title
            if description:
                item.description = description
            if owner:
                owner_user = get_user_by_name_or_email(info, owner)
                if owner_user:
                    item.owner = owner_user.username
                else:
                    return UpdateItemMutation(success=False)
            if holder:
                holder_user = get_user_by_name_or_email(info, holder)
                if holder_user:
                    item.holder = holder_user.username
                else:
                    return UpdateItemMutation(success=False)
            db.session.commit()
            return UpdateItemMutation(success=True)
        return UpdateItemMutation(success=False)


class Mutation(graphene.ObjectType):
    user_password_authentication_mutation = UserPasswordAuthenticationMutation.Field()
    create_item_mutation = CreateItemMutation.Field()
    update_item_mutation = UpdateItemMutation.Field()
    delete_item_mutation = DeleteItemMutation.Field()
    user_registration_request_mutation = UserRegistrationRequestMutation.Field()
    user_registration_mutation = UserRegistrationMutation.Field()


class Query(graphene.ObjectType):
    frontend_menu = graphene.List(FrontendMenuItem)

    my_items = graphene.List(ItemType)
    items = graphene.List(ItemType)
    tags = graphene.List(TagType)

    item = graphene.Field(ItemType, uuid = graphene.ID(required=True))

    who_am_i = graphene.Field(UserType)

    @jwt_required(optional=True)
    def resolve_frontend_menu(self, info, **args):
        menu = [FrontendMenuItem("home")]
        user = current_user
        if not user:
            menu.append(FrontendMenuItem("login"))
        else:
            menu.append(FrontendMenuItem("myItems"))
            menu.append(FrontendMenuItem("items"))
            menu.append(FrontendMenuItem("createItem"))
        return menu

    @jwt_required()
    def resolve_item(self, info, **args):
        uuid = UUID(args.get('uuid'))
        return ItemType.get_query(info).get(uuid)

    @jwt_required()
    def resolve_my_items(self, info, **args):
        user = current_user
        return ItemType.get_query(info).filter(or_(Item.owner==user.username, Item.holder==user.username))

    @jwt_required(optional=True)
    def resolve_items(self, info, **args):
        user = current_user
        if not user:
            user = User(username="anonymous")
        auth_filter = authorize_model(oso, user, "read", db.session, Item)
        return ItemType.get_query(info).filter(auth_filter)

    @jwt_required()
    def resolve_who_am_i(self, info, **args):
        return current_user


schema = graphene.Schema(query=Query, mutation=Mutation)
